fn equals(x: i32, y: i32, z: i32) -> bool {
    x + y == z
}

fn check_divide(x: i32, y: i32, z: i32) -> bool {
    x / y == z
}

fn multiply(x: i32, y: i32) -> i32 {
    x * y
}

#[cfg(test)]
mod tests {
    use super::*;

    // to run single test from terminal execute following command
    // `cargo test it_works`
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn it_equals() {
        assert!(equals(2, 2, 4));
    }

    #[test]
    fn it_fails() {
        assert!(equals(2, 2, 5));
    }

    // this test will be ignored
    #[test]
    #[ignore]
    fn it_panics() {
        panic!("panic!!!");
    }

    #[test]
    fn it_fails_with_message() {
        let a = 2;
        let b = 2;
        let c = 4;
        assert!(check_divide(a, b, c), "custom error");
    }

    #[test]
    #[should_panic]
    fn it_should_panic() {
        let a = 2;
        let b = 2;
        let c = 5;
        assert!(equals(a, b, c), "custom error");
    }

    #[test]
    fn correct() {
        assert_eq!(multiply(3, 3), 9);
    }

    #[test]
    fn incorrect() {
        assert_ne!(multiply(3, 3), 10);
    }
}
