use std::thread;
use std::time::Duration;

fn main() {
    let handle = thread::spawn(|| {
        for i in 1..6 {
            println!("hi1! {}", i);
            thread::sleep(Duration::from_millis(1));
        }
    });
    thread::spawn(|| {
        for i in 1..11 {
            println!("hi2! {}", i);
            thread::sleep(Duration::from_millis(1));
        }
    });
    for _ in 1..3 {
        println!("hello!");
        thread::sleep(Duration::from_millis(1));
    }
    handle.join().unwrap();
}
