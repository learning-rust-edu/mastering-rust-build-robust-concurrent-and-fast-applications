use std::thread;

fn main() {
    let a = 42;

    // this will fail with burrows `a` error
    // let handle = thread::spawn(|| {
    //     println!("{}", a);
    // });

    let handle = thread::spawn(move || {
        println!("{}", a);
    });

    handle.join().unwrap();
}
