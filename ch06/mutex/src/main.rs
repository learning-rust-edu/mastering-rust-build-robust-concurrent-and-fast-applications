use std::thread;
use std::sync::{Arc, Mutex};

fn main() {
    let sv = Arc::new(Mutex::new(1));
    let mut handles = vec![];

    for _ in 0..2 {
        let sv = Arc::clone(&sv);
        let handle = thread::spawn(move || {
            let mut m = sv.lock().unwrap();
            *m *= 2;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }
    println!("m: {}", sv.lock().unwrap());
}
