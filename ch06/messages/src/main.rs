use std::thread;
use std::sync::mpsc;
use std::time::Duration;

fn main() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let a = 5;
        tx.send(a).unwrap();
    });

    let b = rx.recv().unwrap();
    println!("a = {}", b);

    let (tx, rx) = mpsc::channel();
    let handle = thread::spawn(|| {
        random_function(tx);
    });
    handle.join().unwrap();
    let out = rx.recv().unwrap();
    println!("out = {}", out);

    let (tx, rx) = mpsc::channel();
    thread::spawn(move || {
        let nums = vec![1, 2, 3];
        for n in nums {
            tx.send(n).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });
    let mut m = 1;
    for n in rx {
        m *= n;
    }
    println!("m: {}", m);

    let (tx, rx) = mpsc::channel();
    let tx1 = mpsc::Sender::clone(&tx);
    thread::spawn(move || {
        tx1.send(5).unwrap();
    });
    thread::spawn(move || {
        tx.send(6).unwrap();
    });
    let mut m = 1;
    for n in rx {
        m *= n;
    }
    println!("m: {}", m);
}

fn random_function(a: mpsc::Sender<f32>) {
    a.send(5.6).unwrap();
}
