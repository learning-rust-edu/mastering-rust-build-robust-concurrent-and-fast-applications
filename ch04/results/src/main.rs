use std::fs::File;

fn main() {
    let f = File::open("text.txt");
    let m = match f {
        Ok(inf) => inf,
        Err(err) => panic!("There was an error {:?}", err),
    };
}
