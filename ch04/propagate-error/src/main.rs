use std::io::{Error, Read};
use std::fs::File;

fn read_file() -> Result<String, Error> {
    let f = File::open("text.txt");
    let mut m = match f {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut result = String::new();
    match m.read_to_string(&mut result) {
        Ok(_) => Ok(result),
        Err(e) => Err(e),
    }
}

fn read_file_short() -> Result<String, Error> {
    let mut result = String::new();
    File::open("text.txt")?.m.read_to_string(&mut result)?;
    Ok(result)
}

fn main() {
}
