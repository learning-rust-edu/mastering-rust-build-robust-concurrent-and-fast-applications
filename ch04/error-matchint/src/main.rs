use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let filename = "text.txt".to_string();
    let f = File::open(&filename);
    let m = match f {
        Ok(r) => r,
        Err(ref err) if err.kind() == ErrorKind::NotFound => {
            match File::create(&filename) {
                Err(e) => panic!("Cannot create a file: {:?}", e),
                Ok(message) => message,
            }
        },
        Err(e) => panic!("Cannot open file: {:?}", e),
    };
    println!("{:?}", m);
}
