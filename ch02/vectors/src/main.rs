fn main() {
    let mut a = Vec::new();
    a.push(1);
    a.push(1);
    a.push(2);
    println!("{} {} {}", a[0], a[1], a[2]);
    let b = vec![1, 1, 2];
    println!("{} {} {}", b[0], b[1], b[2]);

    match a.get(2) {
        Some(n) => println!("{}", n),
        None => println!("not defined"),
    }

    for n in &b {
        println!("{}", n);
    }

    for n in &mut a {
        *n *= 0;
    }
    for n in &a {
        println!("{}", n);
    }
}
