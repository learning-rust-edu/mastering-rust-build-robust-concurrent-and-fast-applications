enum Temp {
    Hot,
    Normal,
    Cold,
}

impl Temp {
    fn calc_temp(&self) -> i32 {
        match self {
            Temp::Hot => 39,
            Temp::Normal => 36,
            Temp::Cold => 34,
        }
    }
}

fn calc_temp(t: Temp) -> i32 {
    match t {
        Temp::Hot => 39,
        Temp::Normal => 36,
        _ => 34,
    }
}

fn match_value(v: Option<i32>) {
    match v {
        Some(n) => println!("{}", n),
        None => println!("No value"),
    }
}
fn print_value(v: Option<i32>) {
    if let Option::Some(n) = v {
        println!("v = {}", n);
    } else {
        println!("No value");
    }
}

fn main() {
    let temp = Temp::Cold;
    println!("{}", calc_temp(temp));
    let temp2 = Temp::Hot;
    println!("{}", temp2.calc_temp());
    println!();

    let null_var: Option<i32> = None;
    let not_null: Option<i32> = Some(5);
    match_value(null_var);
    match_value(not_null);
    print_value(null_var);
    print_value(not_null);
}
