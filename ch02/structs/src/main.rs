struct Home {
    name: String,
    rooms: i32,
    sold: bool,
}

struct HomeTup(String, i32, bool);

fn make_home(name: String, rooms: i32, sold: bool) -> Home {
    Home {
        name,
        rooms,
        sold,
    }
}

fn price(home: &Home) -> i32 {
    home.rooms * 1000
}

impl Home {
    fn price(&self) -> i32 {
        self.rooms * 2000
    }
    fn bigger_than(&self, h: &Home) -> bool {
        self.rooms > h.rooms
    }
}

fn main() {
    let mut home1 = Home {
        name: "my-home".to_string(),
        rooms: 5,
        sold: false,
    };
    println!("sold before {}", home1.sold);
    home1.sold = true;
    println!("sold after {}", home1.sold);
    let home2 = Home {
        sold: false,
        ..home1
    };
    println!("name2 {}", home2.name);
    println!("rooms2 {}", home2.rooms);
    println!("sold2 {}", home2.sold);
    println!();

    let home3 = HomeTup("my".to_string(), 6, true);
    println!("rooms: {}", home3.1);
    println!();

    let home4 = make_home("myhome".to_string(), 6, false);
    println!("{}", home4.rooms);
    println!("price {}", price(&home4));
    println!("price {}", home4.price());
    println!("{}", home4.bigger_than(&home2));
    println!("{}", home2.bigger_than(&home4));
}
