mod addition;
mod comparison;

use addition::add;
use comparison::compare;

fn main() {
    let x = 5;
    let y = 6;
    let z = 7;
    compare(add(x, z), add(x, y));
}
