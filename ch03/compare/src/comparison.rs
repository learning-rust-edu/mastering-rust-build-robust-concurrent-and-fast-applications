pub fn compare(x: i32, y: i32) {
    if x < y {
        println!("{} < {}", x, y);
    } else if x > y {
        println!("{} > {}", x, y);
    } else {
        println!("{} == {}", x, y);
    }
}
