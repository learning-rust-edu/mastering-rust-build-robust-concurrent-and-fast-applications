mod conversation;

use conversation::hello as h;
use conversation::goodbye::bye;
use conversation::night;

fn main() {
    // when only module is defined then full module path should be used
    conversation::hello::hi();
    conversation::goodbye::bye();
    conversation::night::say();
    // `use` allow to shorten call path
    h::hi();
    bye();
    night::say();
    println!("Hello, world!");
}
