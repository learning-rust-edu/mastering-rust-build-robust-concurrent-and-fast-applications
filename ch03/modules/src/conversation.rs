pub mod hello {
    pub fn hi() {
        println!("hi!");
    }
}
pub mod goodbye {
    pub fn bye() {
        println!("Bye!");
    }
}
pub mod night {
    pub fn say() {
        println!("Night!");
    }
}
